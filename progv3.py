#iterable : tout itérable (liste, tuple, set, dictionnaire, etc.) qui contient les éléments
#la fonction all() renvoie True : Si tous les éléments d’un itérable sont True, False : Si un élément d’un itérable est False
liste = [False, True, True]
response = all(liste)
print(response)

#La fonction any() renvoie true si l’un des éléments de la liste transmise est true

liste1 = []  # liste vide
print(any(liste1)) #renvoie False

liste2 = [10, 0, False] # la 1ère est vraie, les autres sont fausses
print(any(liste2))

#locals() renvoie un dictionnaire contenant les variables définies dans le namespace locale, il ne prend rien en paramètre
print(locals())

# fonction slice() renvoie un objet slice. Un objet slice est utilisé pour spécifier
# comment découper une séquence. Vous pouvez spécifier où commencer le découpage et où se terminer

tuple = ("W", "X", "Y", "Z")
objet = slice(2)
print(tuple[objet])

tupl = ("O", "P", "Q", "R", "S")
obj = slice(2, 4)
print(tupl[obj])