# =====================================================================================================================
# Programme qui calcule le factoriel d'un nombre donné
# Author : Dieudonné NKUNA OBIANG
# =====================================================================================================================

import bcolors
print("|======================================================|")
print("|=========FACTORIELLE D'UN NOMBRE INFERIEUR A 100======|")
print("|======================================================|")


#fonction qui calcule la factorielle d'un nombre

def calculefactorielle():
    try:
        #recupere ce que l'utilisateur a saisi
        nombre = int(input("Veuillez entrer un nombre > "))

        if(nombre <= 100): # si le nombre est inférieur ou egale à 100

            factorielle = 1
            for i in range(1, nombre + 1):
                factorielle = factorielle * i
            print(f"{bcolors.bcolors.OK}La factorielle de {nombre} est : {factorielle} ")

        else:
            print(f"{bcolors.bcolors.FAIL}ERROR : Nombre trop grand ! ")

    except ValueError:
        print(f"{bcolors.bcolors.FAIL}ERROR : Vous n'avez pas entré un nombre ! ")

    refairecalcul()



#fonction qui refait le calcule

def refairecalcul():
    while True:
        usdr = input(f"{bcolors.bcolors.RESET}\nVoulez-vous refaire un autre calcul ? O|N > {bcolors.bcolors.RESET}")
        if usdr == "O" or usdr == "o":
            calculefactorielle()
        elif usdr == "N" or usdr == "n":
            print("\nAu revoir et à la prochaine ")
            exit() # sortie du programme
        else:
            print()


while True:
    try:
        #appel de la fonction pour le calcul de la factorielle
        calculefactorielle()
        break
    except ValueError:
        print(f"{bcolors.bcolors.FAIL}\nERROR : Vous n'avez pas entré un nombre ! ")
    #appel de la fonction pour refaire un autre calcul
    refairecalcul()
