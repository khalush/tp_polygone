print("|=======================================================|")
print("|   PROGRAMME DE TRACAGE DES FIGURES GEOMETRIQUES       |")
print("|=======================================================|")
print("| 1-Tracer un Triange isocèle                           |")
print("| 2-Tracer un Carré                                     |")
print("| 3-Tracer un rectangle                                 |")
print("| 4-Tracer un Cercle                                    |")
print("| 5-Tracer un Cube                                      |")
print("| 6-Tracer un hexagone                                  |")
print("|=======================================================|")

#importation des bibliotheques
import turtle
import time

#inviter l'utilisateur à choisir une option
option = int(input("Veuillez choisir une option : "))
#figures authorisées
figures =[1,2,3,4,5,6]


if(option in figures):

   #Option 1 dessine un triangle equilatéral
    if option == 1:

        while True:
            try:
                cote=int(input("Entrer la longueur du coté : "))
                break;
            except ValueError:
                print("ATTENTION : Votre reponse est incorrecte, veuillez entrer un nombre !")
                print()
        #tracer le triangle
        fenetre = turtle.Screen()
        fenetre.bgcolor("black")
        fenetre.title("DESSINER UNE TRIANGLE")
        stylo = turtle.Turtle()
        stylo.color("green")

        for _ in range(3):
            stylo.forward(cote)
            stylo.left(120)

    # Option 2 dessine un carré
    elif option == 2:

        while True :
            try:
                cote = int(input("Entrer la longueur du coté : "))
                break;
            except ValueError:
                print("ATTENTION : Votre reponse est incorrecte, veuillez entrer un nombre !")
                print()

        fenetre = turtle.Screen()
        fenetre.bgcolor("black")
        fenetre.title("DESSINER UN CARRE")
        stylo = turtle.Turtle()
        stylo.color("green")

        for _ in range(4):
            stylo.forward(cote)
            stylo.left(90)

    # Option 2 dessine un rectangle
    elif option == 3:

        while True:
            try:
                longueur = int(input("Entrer la longueur : "))
                largeur = int(input("Entrer la largeur : "))
                break;
            except ValueError:
                print("ATTENTION : Votre reponse est incorrecte, la longueur et la largeur doivent etre des nombres !! ")
                print()

        fenetre = turtle.Screen()
        fenetre.bgcolor("black")
        fenetre.title("DESSINER UN RECTANGLE")
        stylo = turtle.Turtle()
        stylo.color("green")

        if largeur<longueur:

            for _ in range(4):
                stylo.forward(longueur)
                stylo.left(90)
                stylo.forward(largeur)
                stylo.left(90)
        else:
            for _ in range(4):
                stylo.forward(largeur)
                stylo.left(90)
                stylo.forward(longueur)
                stylo.left(90)

    # Option 3 dessine un cercle
    elif option == 4:
        while True:
            try:
                rayon_cercle = int(input("Entrer le rayon du cercle : "))
                break
            except ValueError:
                print("ATTENTION : Votre reponse est incorrecte, le rayon d'un cercle doit etre un nombre !! ")
                print()

        fenetre = turtle.Screen()
        fenetre.bgcolor("black")
        fenetre.title("DESSINER UN CERCLE")
        stylo = turtle.Turtle()
        stylo.color("green")
        stylo.circle(rayon_cercle)

    # Option 4 dessine un cube 3D
    elif option == 5:

        while True:
            try:
                arrete = int(input("Entrer la longueur de l'arrete : "))
                break
            except ValueError:
                print("ATTENTION : Votre reponse est incorrecte, l'arrete d'un cube est un nombre !! ")
                print()

        fenetre = turtle.Screen()
        fenetre.bgcolor("black")
        fenetre.title("DESSINER UN CUBE 3D")
        stylo = turtle.Turtle()
        stylo.color("green")

        #premier carré
        for _ in range(4):
            stylo.forward(arrete)
            stylo.left(90)
        #on se deplace vers le bas à gauche
        stylo.goto(arrete/2, arrete/2)

        #carré arrière
        for _ in range(4):
            stylo.forward(arrete)
            stylo.left(90)

        #on se deplace vers le bas à droite
        stylo.goto(3*arrete/2, arrete/2)
        stylo.goto(arrete, 0)

        # on se deplace vers le haut à droite
        stylo.goto(arrete, arrete)
        stylo.goto(3*arrete/2, 3*arrete/2)

        #on se deplace vers le haut, puis à gauche
        stylo.goto(arrete/2, 3*arrete/2)
        stylo.goto(0, arrete)

    # Option 6 dessine un hexagone
    elif option == 6:
        while True:
            try:
                cote = int(input("Entrer la longueur du cote : "))
                break
            except ValueError:
                print("ATTENTION : Votre reponse est incorrecte, veuillez rentrer un nombre !! ")
                print()

        fenetre = turtle.Screen()
        fenetre.bgcolor("black")
        fenetre.title("DESSINER UN HEXAGONE")
        stylo = turtle.Turtle()
        stylo.color("green")

        for _ in range(6):
            stylo.forward(cote)
            stylo.left(300)
    time.sleep(5)
else:
    print("Vous n'avez pas entré le bon choix !!")







