# =====================================================================================================================
# Programme qui calcule le factoriel d'un nombre donné
# Author : Dieudonné NKUNA OBIANG
# =====================================================================================================================


print("|======================================================|")
print("|==========CALCUL DU FACTORIEL D4UN NOMBRE=============|")
print("|======================================================|")




# factoriel
while True:

    try:
        nombre = input("Veuillez entrer un nombre : ")
        val=int(nombre)
        factorielle = 1
        for i in range(1, val+1):
            factorielle = factorielle*i
        print(f"Le factoriel de {nombre} est : {factorielle}")
        break
    except ValueError:
        print("ERROR : Vous n'avez pas entré un nombre ! ")
        print()